package com.company.commands.creation;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Member;
import java.util.List;

public class CreateMemberCommand implements Command {
    private static final String MEMBER_CREATED = "Member created";
    private final Factory factory;
    private final Engine engine;

    public CreateMemberCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String name;

        try {
            name = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CREATE_MEMBER));
        }

        Member member = factory.createMember(name);
        member.addActivityHistory(MEMBER_CREATED);
        engine.getMembers().add(member);

        return String.format(CommandConstants.MEMBER_SUCCESSFULLY_CREATED,name, engine.getMembers().size() - 1);
    }
}
