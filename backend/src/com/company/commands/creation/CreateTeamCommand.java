package com.company.commands.creation;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Team;
import java.util.List;

public class CreateTeamCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public CreateTeamCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        String name;

        try {
            name = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CREATE_TEAM));
        }

        Team team = factory.createTeam(name);
        engine.getTeams().add(team);

        return String.format(CommandConstants.TEAM_SUCCESSFULLY_CREATED,name, engine.getTeams().size() - 1);
    }
}
