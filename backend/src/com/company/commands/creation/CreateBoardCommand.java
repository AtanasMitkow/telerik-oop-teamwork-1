package com.company.commands.creation;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Team;

import java.util.List;

public class CreateBoardCommand implements Command {
    private static final String BOARD_CREATED = "Board created";
    private final Factory factory;
    private final Engine engine;

    public CreateBoardCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters,Finder finder) {
        String teamName;
        String boardName;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CREATE_BOARD));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board!=null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        Board newBoard = factory.createBoard(boardName);
        newBoard.addActivityHistory(BOARD_CREATED);
        engine.getBoards().add(newBoard);

        Team team = finder.findTeam(teamName);
        if(team == null) return String.format(CommandConstants.TEAM_DOES_NOT_EXIST,teamName);
        team.addBoard(newBoard);

        return String.format(CommandConstants.BOARD_SUCCESSFULLY_CREATED, boardName, engine.getBoards().size() - 1);
    }
}
