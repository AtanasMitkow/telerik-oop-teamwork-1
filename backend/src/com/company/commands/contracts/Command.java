package com.company.commands.contracts;

import com.company.core.contracts.Finder;

import java.util.List;

public interface Command {
    String execute(List<String> parameters,Finder finder);
}