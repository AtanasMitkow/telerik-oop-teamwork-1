package com.company.commands.listing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Member;
import java.util.List;

public class ShowMembersCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public ShowMembersCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        List<Member> members = engine.getMembers();
        if(members.size() == 0) throw new IllegalArgumentException(CommandConstants.NO_REGISTERED_MEMBERS);

        StringBuilder builder = new StringBuilder();

        builder.append("All members:\n");

        members.forEach(member -> {
            builder.append(member.getName());
            builder.append("\n");
        });

        return builder.toString().trim();
    }
}

