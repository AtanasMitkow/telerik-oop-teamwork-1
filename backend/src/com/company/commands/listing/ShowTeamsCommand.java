package com.company.commands.listing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Team;

import java.util.List;

public class ShowTeamsCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public ShowTeamsCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        List<Team> teams = engine.getTeams();
        if(teams.size() == 0) throw new IllegalArgumentException(CommandConstants.NO_REGISTERED_TEAMS_ERROR);

        StringBuilder builder = new StringBuilder();

        builder.append("All teams:\n");

        teams.forEach(team->{
            builder.append(team.getName());
            builder.append("\n");
        });

        return builder.toString().trim();
    }
}
