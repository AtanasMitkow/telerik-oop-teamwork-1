package com.company.commands.listing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Member;

import java.util.List;

public class ShowActivityOfMemberCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public ShowActivityOfMemberCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        String memberName;

        try {
            memberName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.SHOW_ACTIVITY));
        }

        Member member = finder.findMember(memberName);
        if(member == null) throw new IllegalArgumentException(String.format(CommandConstants.MEMBER_DOES_NOT_EXIST,memberName));

        return member.showActivityHistory();
    }
}
