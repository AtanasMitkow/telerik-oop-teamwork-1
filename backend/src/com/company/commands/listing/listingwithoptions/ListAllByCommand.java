package com.company.commands.listing.listingwithoptions;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.enumerators.Status;
import com.company.models.workItems.contracts.MemberWorkItem;
import com.company.models.workItems.contracts.WorkItem;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListAllByCommand implements Command {
    private static final String STATUS = "STATUS";
    private static final String ASSIGNEE = "ASSIGNEE";
    private static final String NO_MATCHING_ITEMS = "No items that match the criteria!";
    private final Factory factory;
    private final Engine engine;

    public ListAllByCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        String firstCriteria;
        String criteriaType;
        String secondCriteria = "DEFAULT";
        String secondCriteriaType = "DEFAULT";
        try
        {
            firstCriteria = parameters.get(0);
            criteriaType = parameters.get(1);
            if(parameters.size() >=4)
            {
                secondCriteria = parameters.get(2);
                secondCriteriaType = parameters.get(3);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.LIST_ALL_BY));
        }


        List<WorkItem> workItems = new ArrayList<>();
        List<MemberWorkItem> memberWorkItems = new ArrayList<>();

        if(firstCriteria.toUpperCase().equals(STATUS) && secondCriteria.toUpperCase().equals(ASSIGNEE))
        {
            memberWorkItems = engine.getMemberWorkItems();
            memberWorkItems = filterByAssignee(secondCriteriaType,memberWorkItems);
            memberWorkItems = filterByStatus(getStatusType(criteriaType),memberWorkItems);
        }
        else if(firstCriteria.toUpperCase().equals(STATUS))
        {
            workItems = engine.getAllWorkItems();
            workItems = filterByStatus(getStatusType(criteriaType),workItems);
        }
        else if (firstCriteria.toUpperCase().equals(ASSIGNEE))
        {
            memberWorkItems = engine.getMemberWorkItems();
            memberWorkItems = filterByAssignee(criteriaType,memberWorkItems);
        }

        if(workItems.size() != 0) return engine.workItemListToString(workItems);
        else if(memberWorkItems.size() != 0) return engine.workItemListToString(memberWorkItems);
        else return NO_MATCHING_ITEMS;
    }

    private <T extends WorkItem> List<T> filterByStatus(Status status,List<T> itemsToFilter)
    {
        return itemsToFilter.stream().filter(element->element.getStatus() == status).collect(Collectors.toList());
    }

    private List<MemberWorkItem> filterByAssignee(String name,List<MemberWorkItem> list)
    {
        return list.stream()
                .filter(item->item.getAssignee().getName().equals(name))
                .collect(Collectors.toList());
    }

    private Status getStatusType(String status)
    {
        switch (status.toUpperCase())
        {
            case "ACTIVE":
                return Status.Active;
            case "FIXED":
                return Status.Fixed;
            case "NOTDONE":
                return Status.NotDone;
            case "INPROGRESS":
                return Status.InProgress;
            case "DONE":
                return Status.Done;
            case "NEW":
                return Status.New;
            case "UNSCHEDULED":
                return Status.Unscheduled;
            case "SCHEDULED":
                return Status.Scheduled;
            default:
                throw new IllegalArgumentException("Invalid status name");
        }
    }
}
