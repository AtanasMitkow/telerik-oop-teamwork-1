package com.company.commands.listing.listingwithoptions;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.workItems.contracts.WorkItem;
import java.util.ArrayList;
import java.util.List;

public class ListAllByTypeCommand implements Command {
    private static final String NO_ITEMS_OF_TYPE = "There are no items of this type";
    private static final String INVALID_CATEGORY = "No such category of work items";
    private final Factory factory;
    private final Engine engine;

    public ListAllByTypeCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        String criteria;
        try
        {
            criteria = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.LIST_ALL_BY_TYPE));
        }

            List<WorkItem> workItems = new ArrayList<>();

        switch(criteria.toUpperCase())
        {
            case "BUG": workItems.addAll(engine.getBugs()); break;
            case "FEEDBACK": workItems.addAll(engine.getFeedbacks()); break;
            case "STORY": workItems.addAll(engine.getStories()); break;
            default: throw new IllegalArgumentException(INVALID_CATEGORY);
        }

        if(workItems.size() == 0) throw new IllegalArgumentException(NO_ITEMS_OF_TYPE);

        return engine.workItemListToString(workItems);
    }
}
