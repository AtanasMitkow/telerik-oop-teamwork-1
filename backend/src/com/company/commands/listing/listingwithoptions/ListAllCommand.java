package com.company.commands.listing.listingwithoptions;

import com.company.commands.contracts.Command;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.workItems.contracts.WorkItem;
import java.util.List;

public class ListAllCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public ListAllCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        List<WorkItem> workItems = engine.getAllWorkItems();

        return engine.workItemListToString(workItems);
    }
}
