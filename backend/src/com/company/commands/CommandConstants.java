package com.company.commands;

public class CommandConstants {
    public static final String FAILED_TO_PARSE_INPUT = "Failed to parse %s command parameters.";
    public static final String TEAM_DOES_NOT_EXIST = "Team with name %s does not exist";
    public static final String BOARD_DOES_NOT_EXIST = "Board with name %s does not exist in team %s";
    public static final String STORY_CREATED = "Story created";
    public static final String SUCCESSFULLY_CREATED_STORY = "Story with name %s created";
    public static final String BUG_CREATED = "Bug created";
    public static final String SUCCESSFULLY_CREATED_BUG = "Bug with name %s created.";
    public static final String FEEDBACK_CREATED = "FeedBack created";
    public static final String SUCCESSFULLY_CREATED_FEEDBACK = "FeedBack with name %s created";
    public static final String MEMBER_DOES_NOT_EXIST = "Member with name %s does not exist";
    public static final String NO_REGISTERED_TEAMS_ERROR = "There are no teams registered";
    public static final String NO_REGISTERED_MEMBERS = "There are no members registered";
    public static final String BOARD_SUCCESSFULLY_CREATED = "Board with name %s and  ID %d was created.";
    public static final String MEMBER_SUCCESSFULLY_CREATED = "Member with name %s and ID %d was created.";
    public static final String TEAM_SUCCESSFULLY_CREATED = "Team with name %s and ID %d was created.";
    public static final String ITEM_DOES_NOT_EXIST = "Item with name %s does not exist in board %s";
    public static final String SUCCESSFULLY_CHANGED_PRIORITY = "Priority of item %s changed to %s";
    public static final String SUCCESSFULLY_CHANGED_SEVERITY = "Severity of item %s changed to %s";
    public static final String SUCCESSFULLY_CHANGED_SIZE = "Size of item %s changed to %s";
    public static final String MEMBER_COMMENT_HISTORY_STRING = "%s commented on work item %s in board %s";
}
