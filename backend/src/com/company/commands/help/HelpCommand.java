package com.company.commands.help;

import com.company.commands.contracts.Command;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Member;

import java.util.List;

public class HelpCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public HelpCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        return ("List of commands:\n" +
                "Creation commands:\n" +

                "createmember + (name of the member)\n" +
                "createteam + (name of the team)\n" +
                "createboard + (name of the board)\n" +

                "Showing commands\n" +

                "showmembers\n" +
                "showactivity + (name of the member)\n" +
                "showteams\n" +
                "showteammembers + (name of the team)\n" +
                "showteamactivity + (name of the team)\n" +
                "showteamboards + (name of the team)\n" +
                "showboardactivity + (team) + (board)\n" +

                "Showing commands with criteria" +

                "listallworkitems\n" +
                "listallbytype + (type of work item)\n" +
                "listallby + (status/assignee criteria) + (name of criteria) + {Optional}(assignee criteria) + {name of assignee)\n" +

                "Adding commands:\n" +

                "addmember + (name of the member) + (name of the team)\n" +
                "assignmember + (name of the team) + (name of the board) + (name of the item) + (name of the member)\n" +
                "unassignmember + (name of the team) + (name of the board) + (name of the item)\n" +
                "addcomment + (name of the team) + (name of the board) + (name of the item) + (name of the member) + (comment text)\n" +

                "Work Item creation:\n" +

                "createbug + (team) + (board) + (title of the bug) + (description)\n" +
                "createstory + (team) + (board) + (title of the story) + (description)\n" +
                "createfeedback + (team) + (board) + (title of the feedback) + (description) + (rating)\n" +

                "Changing commands:\n" +

                "changestatus + (name of the team) + (name of the board) + (name of the item) + (name of the status)\n" +
                "changepriority + (name of the team) + (name of the board) + (name of the item) + (name of the priority)\n" +
                "changeseverity + (name of the team) + (name of the board) + (name of the item) + (name of the severity)\n" +
                "changerating + (name of the team) + (name of the board) + (name of the item) + (rating value)\n" +
                "changesize + (name of the team) + (name of the board) + (name of the item) + (name of the size)\n" +

                "Sorting commands:\n" +

                "sortitemsbytitle\n" +
                "sortitemsbysize\n" +
                "sortitemsbyseverity\n" +
                "sortitemsbyrating\n" +
                "sortitemsbypriorty"
                );
    }
}