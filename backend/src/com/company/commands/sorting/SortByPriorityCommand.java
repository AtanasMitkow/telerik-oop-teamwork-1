package com.company.commands.sorting;

import com.company.commands.contracts.Command;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.workItems.contracts.MemberWorkItem;

import java.util.List;

public class SortByPriorityCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public SortByPriorityCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        List<MemberWorkItem> items = engine.getMemberWorkItems();
        StringBuilder builder = new StringBuilder();
        items.stream().sorted((o1, o2)->o1.getPriority().
                compareTo(o2.getPriority())).
                forEach(item->builder.append(item.toString()));

        return builder.toString().trim();
    }
}
