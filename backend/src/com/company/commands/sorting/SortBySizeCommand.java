package com.company.commands.sorting;

import com.company.commands.contracts.Command;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.workItems.contracts.Story;
import java.util.List;

public class SortBySizeCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public SortBySizeCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        List<Story> items = engine.getStories();
        StringBuilder builder = new StringBuilder();
        items.stream().sorted((o1, o2)->o1.getSize().
                compareTo(o2.getSize())).
                forEach(item->builder.append(item.toString()));

        return builder.toString().trim();
    }
}
