package com.company.commands.changing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.enumerators.Size;
import com.company.models.team.contracts.Board;
import com.company.models.workItems.contracts.Story;

import java.util.List;

public class ChangeSizeCommand implements Command {
    private static final String SIZE_CHANGED = "Size changed to %s";
    private final Engine engine;
    private final Factory factory;

    public ChangeSizeCommand(Factory factory,Engine engine)
    {
        this.engine = engine;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String itemName;
        String sizeName;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);
            sizeName = parameters.get(3);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CHANGE_SIZE));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        Story item = finder.findItemInCollection(board.getStories(),itemName);
        if(item == null) throw new IllegalArgumentException(String.format(CommandConstants.ITEM_DOES_NOT_EXIST,
                itemName,boardName));

        item.changeSize(getSize(sizeName));
        item.addHistory(String.format(SIZE_CHANGED,item.getSize()));

        return String.format(CommandConstants.SUCCESSFULLY_CHANGED_SIZE,itemName,item.getSize());
    }

    private Size getSize(String size)
    {
        switch(size.toUpperCase())
        {
            case "LARGE":
                return Size.Large;
            case "MEDIUM":
                return Size.Medium;
            case "SMALL":
                return Size.Small;
            default:
                throw new IllegalArgumentException("Invalid ssize name");
        }
    }
}
