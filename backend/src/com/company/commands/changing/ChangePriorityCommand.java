package com.company.commands.changing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.enumerators.Priority;
import com.company.models.team.contracts.Board;
import com.company.models.workItems.contracts.MemberWorkItem;

import java.util.List;

public class ChangePriorityCommand implements Command {
    private static final String PRIORITY_CHANGED = "Priority changed to %s";
    private final Factory factory;
    private final Engine engine;

    public ChangePriorityCommand(Factory factory,Engine engine)
    {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String itemName;
        String priorityName;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);
            priorityName = parameters.get(3);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CHANGE_PRIORITY));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        MemberWorkItem item = finder.findItemInCollection(board.getMemberWorkItems(),itemName);
        if(item == null) throw new IllegalArgumentException(String.format(CommandConstants.ITEM_DOES_NOT_EXIST,
                itemName,boardName));

        item.changePriority(getPriority(priorityName));
        item.addHistory(String.format(PRIORITY_CHANGED,item.getPriority()));

        return String.format(CommandConstants.SUCCESSFULLY_CHANGED_PRIORITY,itemName,item.getPriority());
    }

    private Priority getPriority(String priority)
    {
        switch (priority.toUpperCase())
        {
            case "HIGH":
                return Priority.High;
            case "MEDIUM":
                return Priority.Medium;
            case "LOW":
                return Priority.Low;
            default:
                throw new IllegalArgumentException("Invalid priority name");
        }
    }
}
