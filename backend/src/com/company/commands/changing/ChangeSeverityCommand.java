package com.company.commands.changing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.enumerators.Severity;
import com.company.models.team.contracts.Board;
import com.company.models.workItems.contracts.Bug;
import java.util.List;

public class ChangeSeverityCommand implements Command {
    private static final String SEVERITY_CHANGED = "Severity changed to %s";
    private final Engine engine;
    private final Factory factory;

    public ChangeSeverityCommand(Factory factory,Engine engine)
    {
        this.engine = engine;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String itemName;
        String severityName;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);
            severityName = parameters.get(3);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CHANGE_SEVERITY));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        Bug item = finder.findItemInCollection(board.getBugs(),itemName);
        if(item == null) throw new IllegalArgumentException(String.format(CommandConstants.ITEM_DOES_NOT_EXIST,
                itemName,boardName));

        item.changeSeverity(getSeverity(severityName));
        item.addHistory(String.format(SEVERITY_CHANGED,item.getSeverity()));

        return String.format(CommandConstants.SUCCESSFULLY_CHANGED_SEVERITY,itemName,item.getSeverity());
    }

    private Severity getSeverity(String severity)
    {
        switch(severity.toUpperCase())
        {
            case "CRITICAL":
                return Severity.Critical;
            case "MAJOR":
                return Severity.Major;
            case "MINOR":
                return Severity.Minor;
                default:
                    throw new IllegalArgumentException("Invalid severity name");
        }
    }
}
