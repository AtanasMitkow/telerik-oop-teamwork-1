package com.company.commands.changing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.enumerators.Status;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.WorkItem;
import java.util.List;

public class ChangeStatusCommand implements Command {
    private static final String STATUS_CHANGED = "Status changed to %s";
    private static final String SUCCESSFULLY_CHANGED_STATUS = "Status of item %s changed to %s";
    private final Factory factory;
    private final Engine engine;

    public ChangeStatusCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        String teamName;
        String boardName;
        String itemName;
        String statusName;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);
            statusName = parameters.get(3);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CHANGE_STATUS));
        }

        Team team = finder.findTeam(teamName);
        if(team == null) throw new IllegalArgumentException(String.format(CommandConstants.TEAM_DOES_NOT_EXIST,teamName));

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        WorkItem item = finder.findWorkItemInBoard(board,itemName);
        if(item == null) throw new IllegalArgumentException(String.format(CommandConstants.ITEM_DOES_NOT_EXIST,
                itemName,boardName));

        item.changeStatus(getStatusType(statusName));
        item.addHistory(String.format(STATUS_CHANGED,item.getStatus()));

        return String.format(SUCCESSFULLY_CHANGED_STATUS,itemName,item.getStatus());
    }

    private Status getStatusType(String status)
    {
        switch (status.toUpperCase())
        {
            case "ACTIVE":
                return Status.Active;
            case "FIXED":
                return Status.Fixed;
            case "NOTDONE":
                return Status.NotDone;
            case "INPROGRESS":
                return Status.InProgress;
            case "DONE":
                return Status.Done;
            case "NEW":
                return Status.New;
            case "UNSCHEDULED":
                return Status.Unscheduled;
            case "SCHEDULED":
                return Status.Scheduled;
            default:
                throw new IllegalArgumentException("Invalid status name");
        }
    }
}
