package com.company.commands.changing;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Board;
import com.company.models.workItems.contracts.Feedback;

import java.util.List;

public class ChangeRatingCommand implements Command {
    private static final String RATING_CHANGED = "Rating of item %s changed to %d";
    private static final String SUCCESSFULLY_CHANGED_RATING = "Rating of item %s changed to %d";
    private final Factory factory;
    private final Engine engine;

    public ChangeRatingCommand(Factory factory,Engine engine)
    {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String itemName;
        int rating;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);
            rating = Integer.parseInt(parameters.get(3));
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CHANGE_RATING));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        Feedback item = finder.findItemInCollection(board.getFeedbacks(),itemName);
        if(item == null) throw new IllegalArgumentException(String.format(CommandConstants.ITEM_DOES_NOT_EXIST,
                itemName,boardName));

        item.changeRating(rating);
        item.addHistory(String.format(RATING_CHANGED,itemName,rating));

        return String.format(SUCCESSFULLY_CHANGED_RATING,itemName,rating);
    }
}
