package com.company.commands.adding;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;

import java.util.List;

public class AddMemberToTeamCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public AddMemberToTeamCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String memberName;
        String teamName;

        try {
            memberName = parameters.get(0);
            teamName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.ADD_MEMBER));
        }

        Member member = finder.findMember(memberName);
        if (member == null) throw new IllegalArgumentException(String.format("Member with name %s does not exist", memberName));

        Team team = finder.findTeam(teamName);
        if(team == null) throw new IllegalArgumentException(String.format(CommandConstants.TEAM_DOES_NOT_EXIST,teamName));

        Member member2 = finder.findMemberInTeam(memberName,teamName);
        if (member2 != null) throw new IllegalArgumentException(String.format("Member with name %s alredy in team %s",
                memberName, teamName));

        team.addMember(member);

        member.addActivityHistory( String.format("%s was added to team %s.", memberName, teamName));

        return String.format("Member with name %s was added to team %s.", memberName, teamName);
    }
}
