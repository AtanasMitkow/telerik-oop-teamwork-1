package com.company.commands.adding;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.workItems.contracts.Comment;
import com.company.models.workItems.contracts.WorkItem;

import java.util.List;

public class AddCommentCommand implements Command {
    private static final String COMMENT_ADDED = "Comment added";
    private static final String MEMBER_COMMENTED_HISTORY = "%s commented on this item";
    private final Factory factory;
    private final Engine engine;

    public AddCommentCommand(Factory factory,Engine engine)
    {
        this.factory = factory;
        this.engine = engine;
    }
    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String itemName;
        String memberName;
        String comment;

        try
        {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);
            memberName = parameters.get(3);
            comment = buildComment(parameters);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.ADD_COMMENT));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        WorkItem item = finder.findWorkItemInBoard(board,itemName);
        if(item == null) throw new IllegalArgumentException(String.format(CommandConstants.ITEM_DOES_NOT_EXIST,
                itemName,boardName));

        Member member = finder.findMember(memberName);
        if(member == null) throw new IllegalArgumentException(String.format(CommandConstants.MEMBER_DOES_NOT_EXIST,memberName));

        Comment com = factory.createComment(memberName,comment);
        item.addComment(com);
        item.addHistory(String.format(MEMBER_COMMENTED_HISTORY,com.getAuthor()));
        member.addActivityHistory( String.format(CommandConstants.MEMBER_COMMENT_HISTORY_STRING,memberName,itemName,boardName));
        return COMMENT_ADDED;
    }

    private String buildComment(List<String> parameters)
    {
        StringBuilder comment = new StringBuilder();
        for(int i = 1;i<parameters.size();++i)
        {
            comment.append(parameters.get(i));
            comment.append(" ");
        }
        return comment.toString().trim();
    }
}
