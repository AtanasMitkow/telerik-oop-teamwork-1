package com.company.commands.adding;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.workItems.contracts.MemberWorkItem;
import java.util.List;

public class AssignCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public AssignCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        String teamName;
        String boardName;
        String itemName;
        String memberName;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);
            memberName = parameters.get(3);

        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.ASSIGN_MEMBER));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        MemberWorkItem item = finder.findAssignableItem(board,itemName);
        if(item == null) throw new IllegalArgumentException("Item not found");

        Member member = finder.findMember(memberName);
        if(member == null) throw new IllegalArgumentException(String.format("Member with name %s does not exist",memberName));

        item.assignMember(member);
        member.addActivityHistory( String.format("%s assigned to work item %s in board %s",memberName,itemName,boardName));
        item.addHistory(String.format("Assigned to member %s",memberName));

        return String.format("Member %s assigned to work item %s in board %s",memberName,itemName,boardName);
    }
}
