package com.company.commands.adding;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.MemberImpl;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.workItems.contracts.MemberWorkItem;

import java.util.List;

public class UnassignCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public UnassignCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder ) {
        String teamName;
        String boardName;
        String itemName;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            itemName = parameters.get(2);

        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.UNASSIGN_MEMBER));
        }

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        MemberWorkItem item = finder.findAssignableItem(board,itemName);
        if(item == null) throw new IllegalArgumentException("Item not found");

        Member defaultAssignee = new MemberImpl("Assignee");

        item.assignMember(defaultAssignee);
        item.addHistory("Removed assignee");

        return String.format("Member unassigned from work item %s in board %s",itemName,boardName);
    }
}
