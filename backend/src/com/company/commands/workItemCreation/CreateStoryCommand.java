package com.company.commands.workItemCreation;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.Story;
import java.util.List;

public class CreateStoryCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public CreateStoryCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String storyName;
        String storyDesc;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            storyName = parameters.get(2);
            storyDesc = parameters.get(3);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CREATE_STORY));
        }

        Team team = finder.findTeam(teamName);
        if(team == null) throw new IllegalArgumentException(String.format(CommandConstants.TEAM_DOES_NOT_EXIST,teamName));

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        Story story = factory.createStory(storyName,storyDesc);
        story.addHistory(CommandConstants.STORY_CREATED);
        board.addStory(story);
        engine.getStories().add(story);
        board.addActivityHistory(String.format(CommandConstants.SUCCESSFULLY_CREATED_STORY,storyName));

        return String.format(CommandConstants.SUCCESSFULLY_CREATED_STORY, storyName);
    }
}
