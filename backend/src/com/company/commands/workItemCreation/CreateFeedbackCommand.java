package com.company.commands.workItemCreation;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.Feedback;
import java.util.List;

public class CreateFeedbackCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public CreateFeedbackCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String feedbackName;
        String feedbackDesc;
        int rating;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            feedbackName = parameters.get(2);
            feedbackDesc = parameters.get(3);
            rating = Integer.parseInt(parameters.get(4));
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CREATE_FEEDBACK));
        }

        Team team = finder.findTeam(teamName);
        if(team == null) throw new IllegalArgumentException(String.format(CommandConstants.TEAM_DOES_NOT_EXIST,teamName));

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        Feedback feedback = factory.createFeedback(feedbackName,feedbackDesc,rating);
        feedback.addHistory(CommandConstants.FEEDBACK_CREATED);
        board.addFeedback(feedback);
        engine.getFeedbacks().add(feedback);
        board.addActivityHistory(String.format(CommandConstants.SUCCESSFULLY_CREATED_FEEDBACK,feedbackName));

        return String.format(CommandConstants.SUCCESSFULLY_CREATED_FEEDBACK, feedbackName);
    }

}
