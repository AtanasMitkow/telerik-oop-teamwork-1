package com.company.commands.workItemCreation;

import com.company.commands.CommandConstants;
import com.company.commands.contracts.Command;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.Bug;
import java.util.List;

public class CreateBugCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public CreateBugCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters, Finder finder) {
        String teamName;
        String boardName;
        String bugName;
        String bugDesc;

        try {
            teamName = parameters.get(0);
            boardName = parameters.get(1);
            bugName = parameters.get(2);
            bugDesc = parameters.get(3);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(CommandConstants.FAILED_TO_PARSE_INPUT, CommandNameConstants.CREATE_BUG));
        }

        Team team = finder.findTeam(teamName);
        if(team == null) throw new IllegalArgumentException(String.format(CommandConstants.TEAM_DOES_NOT_EXIST,teamName));

        Board board = finder.findBoardInTeam(boardName,teamName);
        if(board == null) throw new IllegalArgumentException(String.format(CommandConstants.BOARD_DOES_NOT_EXIST,
                boardName,teamName));

        Bug bug = factory.createBug(bugName,bugDesc);
        bug.addHistory(CommandConstants.BUG_CREATED);
        engine.getBugs().add(bug);
        board.addBug(bug);
        board.addActivityHistory(String.format(CommandConstants.SUCCESSFULLY_CREATED_BUG,bugName));

        return String.format(CommandConstants.SUCCESSFULLY_CREATED_BUG, bugName);
    }


}