package com.company.models.workItems;

import com.company.models.workItems.contracts.Comment;
import com.company.models.workItems.contracts.WorkItem;
import com.company.enumerators.Status;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class WorkItemBase implements WorkItem {
    private static final String TITLE_ERROR = "Title should be between %d and %d symbols.";
    private static final int TITLE_MIN_LENGTH = 10;
    private static final int TITLE_MAX_LENGTH = 50;
    private static final int DESCRIPTION_MIN_LENGTH = 10;
    private static final int DESCRIPTION_MAX_LENGTH = 500;
    private static final String DESCRIPTION_ERROR = "Description should be between %d and %d symbols.";
    private static final String PRINT_FORMAT = "ID: %s,\n" +
                    "Title: %s,\n" +
                    "Description: %s\n" +
                    "Status: %s\n" +
                    "Comments: %s\n" +
                    "History: %s\n" +
                    "%s";
    private static final String UNSET = "Unset";
    private static final String UNSET_ERROR = "Status is not set";

    private final String ID;
    private String title;
    private String description;
    private Status status;
    private List<Comment> comments;
    private List<String> history;

    WorkItemBase(String title, String description) {
        this.ID = UUID.randomUUID().toString();
        setTitle(title);
        setDescription(description);
        this.comments = new ArrayList<>();
        this.history = new ArrayList<>();
        setStatus(Status.Unset);
    }

    private void setTitle(String title) {
        if (title.length() < TITLE_MIN_LENGTH || title.length() > TITLE_MAX_LENGTH ) {
            throw new IllegalArgumentException(getTitleIllegalArgumentMessage());
        }
        this.title = title;
    }

    private void setDescription(String description) {
        if (description.length() < DESCRIPTION_MIN_LENGTH || description.length() > DESCRIPTION_MAX_LENGTH) {
            throw new IllegalArgumentException(getDescriptionIllegalArgumentMessage());
        }
        this.description = description;
    }

    public void addComment(Comment comment)
    {
        this.comments.add(comment);
    }

    private static String getTitleError() {
        return TITLE_ERROR;
    }

    private String getTitleIllegalArgumentMessage() {
        return String.format(getTitleError(), TITLE_MIN_LENGTH, TITLE_MAX_LENGTH);
    }

    private static String getDescriptionError() {
        return DESCRIPTION_ERROR;
    }

    private String getDescriptionIllegalArgumentMessage() {
        return String.format(getDescriptionError(), DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH);
    }

    void setStatus(Status status) {
        this.status = status;
    }

    public abstract void changeStatus(Status status);

    @Override
    public String getID() {
        return this.ID;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public Status getStatus() {
        return this.status;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(this.comments);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(this.history);
    }

    public void addHistory(String history)
    {
        this.history.add(history);
    }

    protected abstract String additionalInfo();

    @Override
    public String toString()
    {
        return String.format(PRINT_FORMAT,
                this.ID,
                this.title,
                this.description,
                this.status.toString().equals(UNSET) ? UNSET_ERROR : this.status.toString(),
                this.comments,
                this.history,
                additionalInfo());
    }

    String getUnset()
    {
        return UNSET;
    }

    String getUnsetMessage(String field)
    {
        return String.format("%s is not set",field);
    }
}

