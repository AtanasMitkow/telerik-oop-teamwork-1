package com.company.models.workItems;

import com.company.models.workItems.contracts.Bug;
import com.company.enumerators.Severity;
import com.company.enumerators.Status;
import java.util.ArrayList;
import java.util.List;

public class BugImpl extends MemberWorkItemBase implements Bug {
    private static final String INVALID_STATUS = "Status is not valid";
    private static final String SEVERITY_STATUS = "Severity is not valid";
    private static final String PRINT_FORMAT = "\nSteps to reproduce: %s\n" +
                    "Severity: %s";
    private List<String> stepsToReproduce;
    private Severity severity;

    public BugImpl(String title, String description) {
        super(title, description);
        this.stepsToReproduce = new ArrayList<>();
        setSeverity(Severity.Unset);
    }

    private void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public void changeSeverity(Severity severity) {
        switch (severity) {
            case Major:
                setSeverity(severity); break;
            case Minor:
                setSeverity(severity); break;
            case Critical:
                setSeverity(severity); break;
            default:
                    throw new IllegalArgumentException(SEVERITY_STATUS);
        }
    }

    public void changeStatus(Status status) {
        switch (status) {
            case Active:
                setStatus(status); break;
            case Fixed:
                setStatus(status); break;
            default:
                throw new IllegalArgumentException(INVALID_STATUS);
        }
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public Severity getSeverity() {
        return this.severity;
    }

    @Override
    protected String getMemberWorkItemInfo() {
        return String.format(PRINT_FORMAT,
                this.stepsToReproduce,
                this.severity.toString().equals(getUnset()) ? getUnsetMessage("Severity") : this.severity.toString());
    }
}
