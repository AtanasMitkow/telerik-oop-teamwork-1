package com.company.models.workItems;

import com.company.models.team.MemberImpl;
import com.company.models.workItems.contracts.MemberWorkItem;
import com.company.models.team.contracts.Member;
import com.company.enumerators.Priority;

public abstract class MemberWorkItemBase extends WorkItemBase implements MemberWorkItem {
    private static final String PRIORITY_STATUS = "Priority is not valid";
    private static final String PRINT_FORMAT = "Assignee: %s\n" +
                    "Priority: %s" +
                    "%s";
    private static final String DEFAULT_ASSIGNEE_NAME = "Assignee";
    private static final String NO_ASSIGNEE_ERROR = "No member assigned";
    private Member assignee;
    private Priority priority;

    MemberWorkItemBase(String title, String description) {
        super(title, description);
        setPriority(Priority.Unset);
        Member defaultAssignee = new MemberImpl(DEFAULT_ASSIGNEE_NAME);
        setAssignee(defaultAssignee);
    }

    private void setAssignee(Member assignee) {
        this.assignee = assignee;
    }

    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void changePriority(Priority priority) {
        switch (priority) {
            case High:
                setPriority(priority); break;
            case Low:
                setPriority(priority); break;
            case Medium:
                setPriority(priority); break;
            default:
                throw new IllegalArgumentException(PRIORITY_STATUS);
        }
    }

    @Override
    public void assignMember(Member member)
    {
        setAssignee(member);
    }

    @Override
    public Member getAssignee() {
        return this.assignee;
    }

    @Override
    public Priority getPriority() {
        return this.priority;
    }

    @Override
    protected String additionalInfo()
    {
        return String.format(PRINT_FORMAT,
                this.assignee.getName().equals(DEFAULT_ASSIGNEE_NAME) ? NO_ASSIGNEE_ERROR : this.assignee.getName(),
                this.priority.toString().equals(getUnset()) ? getUnsetMessage("Priority") : this.priority.toString(),
                getMemberWorkItemInfo());
    }

    protected abstract String getMemberWorkItemInfo();
}
