package com.company.models.workItems;

import com.company.models.workItems.contracts.Story;
import com.company.enumerators.Size;
import com.company.enumerators.Status;

public class StoryImpl extends MemberWorkItemBase implements Story {
    private static final String INVALID_STATUS = "Status is not valid";
    private static final String INVALID_SIZE = "Size is not valid";
    private static final String PRINT_FORMAT = "\nSize: %s\n";
    private Size size;

    public StoryImpl(String title, String description) {
        super(title, description);
        setSize(Size.Unset);
    }

    private void setSize(Size size) {
        this.size = size;
    }

    public void changeSize(Size size) {
        switch (size) {
            case Medium:
                setSize(size); break;
            case Large:
                setSize(size); break;
            case Small:
                setSize(size); break;
            default:
                throw new IllegalArgumentException(INVALID_SIZE);
        }
    }

    public void changeStatus(Status status) {
        switch (status) {
            case NotDone:
                setStatus(status); break;
            case InProgress:
                setStatus(status); break;
            case Done:
                setStatus(status); break;
            default:
                throw new IllegalArgumentException(INVALID_STATUS);
        }
    }

    @Override
    public Size getSize() {
        return this.size;
    }

    @Override
    protected String getMemberWorkItemInfo() {
        return String.format(PRINT_FORMAT,
                            this.size.toString().equals(getUnset()) ? getUnsetMessage("Size") : this.size.toString());
    }
}
