package com.company.models.workItems;

import com.company.models.workItems.contracts.Feedback;
import com.company.enumerators.Status;

public class FeedbackImpl extends WorkItemBase implements Feedback {
    private static final String INVALID_STATUS = "Status is not valid";
    private static final int MAX_RATING = 100;
    private static final String NEGATIVE_RATING_ERROR = "Rating can not be negative";
    private static final String PRINT_FORMAT = "Rating: %d\n";
    private int rating;

    public FeedbackImpl(String title, String description, int rating) {
        super(title, description);
        setRating(rating);
    }

    private void setRating(int rating) {
        if(rating<1 || rating > MAX_RATING) throw new IllegalArgumentException(NEGATIVE_RATING_ERROR);
        this.rating = rating;
    }

    public void changeRating(int rating) {
        setRating(rating);
    }

    @Override
    public int getRating() {
        return this.rating;
    }

    @Override
    public void changeStatus(Status status) {
        switch (status) {
            case Unscheduled:
                setStatus(status); break;
            case Scheduled:
                setStatus(status); break;
            case Done:
                setStatus(status); break;
            default:
                throw new IllegalArgumentException(INVALID_STATUS);
        }
    }

    @Override
    protected String additionalInfo() {
        return String.format(PRINT_FORMAT,this.rating);
    }
}
