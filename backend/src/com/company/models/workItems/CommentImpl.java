package com.company.models.workItems;

import com.company.models.workItems.contracts.Comment;

public class CommentImpl implements Comment {
    private static final String PRINT_FORMAT = "\nAuthor: %s\n" +
            "Comment: %s\n";
    private String author;
    private String comment;

    public CommentImpl(String author, String comment) {
        this.author = author;
        this.comment = comment;
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    private void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthor() {
        return author;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return String.format(PRINT_FORMAT,author,comment);
    }
}
