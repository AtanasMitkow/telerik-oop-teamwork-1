package com.company.models.workItems.contracts;

import com.company.enumerators.Severity;

import java.util.List;

public interface Bug extends MemberWorkItem {
    List<String> getStepsToReproduce();
    Severity getSeverity();
    void changeSeverity(Severity severity);
}
