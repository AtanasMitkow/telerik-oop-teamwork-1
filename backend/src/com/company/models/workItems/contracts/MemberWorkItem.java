package com.company.models.workItems.contracts;

import com.company.models.team.contracts.Member;
import com.company.enumerators.Priority;

public interface MemberWorkItem extends WorkItem {
    Member getAssignee();
    Priority getPriority();
    void assignMember(Member member);
    void changePriority(Priority priority);
}
