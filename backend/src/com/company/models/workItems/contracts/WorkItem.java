package com.company.models.workItems.contracts;

import com.company.enumerators.Status;

import java.util.List;

public interface WorkItem {
    String getID();
    String getTitle();
    String getDescription();
    Status getStatus();
    List<Comment> getComments();
    List<String> getHistory();
    void changeStatus(Status status);
    void addHistory(String history);
    void addComment(Comment comment);
}
