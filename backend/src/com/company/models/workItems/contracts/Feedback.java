package com.company.models.workItems.contracts;

public interface Feedback extends WorkItem{
    int getRating();
    void changeRating(int rating);
}
