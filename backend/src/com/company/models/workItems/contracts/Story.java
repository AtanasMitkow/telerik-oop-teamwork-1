package com.company.models.workItems.contracts;

import com.company.enumerators.Size;

public interface Story extends MemberWorkItem {
    Size getSize();
    void changeSize(Size size);

}
