package com.company.models.workItems.contracts;

public interface Comment {
    String getAuthor();
    String getComment();
}
