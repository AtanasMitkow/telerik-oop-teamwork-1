package com.company.models.team;

import com.company.models.team.contracts.Board;

public class BoardImpl extends TeamUnitBase implements Board {
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 10;
    private static final String NAME_ERROR = "Name should be between %d and %d symbols.";

    public BoardImpl(String name) {
        super(name);
    }

    int getMinNameLength() {
        return  NAME_MIN_LENGTH;
    }

    int getMaxNameLength() {
        return  NAME_MAX_LENGTH;
    }

    protected String getNameIllegalArgumentMessage() {
        return String.format(NAME_ERROR, getMinNameLength(), getMaxNameLength());
    }
}
