package com.company.models.team;

import com.company.models.workItems.contracts.*;
import com.company.models.team.contracts.TeamUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class TeamUnitBase implements TeamUnit {
    private static final int NAME_MIN_LENGTH = 1;
    private static final int NAME_MAX_LENGTH = 32;
    private static final String NAME_ERROR = "Name can not be less than %d and more than %d symbols";
    private static final String NO_MEMBER_ACTIVITY_HISTORY = "Member %s does not have activity history";
    private static final String USER_HAS_NO_WORK_ITEMS = "User %s has no work items";

    private String name;
    private List<Bug> bugs;
    private List<Story> stories;
    private List<Feedback> feedbacks;
    private List<String> activityHistory;

    TeamUnitBase(String name) {
        setName(name);
        this.bugs = new ArrayList<>();
        this.stories = new ArrayList<>();
        this.feedbacks = new ArrayList<>();
        this.activityHistory = new ArrayList<>();
    }

    private void setName(String name) {
        if (name.length() < getMinNameLength() || name.length() > getMaxNameLength() || name.isEmpty()) {
            throw new IllegalArgumentException(getNameIllegalArgumentMessage());
        }
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<WorkItem> getWorkItems() {
        ArrayList<WorkItem> returnList = new ArrayList<>(bugs);
        returnList.addAll(stories);
        returnList.addAll(feedbacks);
        return returnList;
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    public void addBug(Bug bug)
    {
        this.bugs.add(bug);
    }

    public void addStory(Story story)
    {
        this.stories.add(story);
    }

    public void addFeedback(Feedback feedback)
    {
        this.feedbacks.add(feedback);
    }

    public List<Bug> getBugs()
    {
        return new ArrayList<>(bugs);
    }

    public List<Story> getStories()
    {
        return new ArrayList<>(stories);
    }

    public List<Feedback> getFeedbacks()
    {
        return new ArrayList<>(feedbacks);
    }

    public List<MemberWorkItem> getMemberWorkItems()
    {
        ArrayList<MemberWorkItem> returnList = new ArrayList<>(bugs);
        returnList.addAll(stories);
        return returnList;
    }

    int getMinNameLength() {
        return NAME_MIN_LENGTH;
    }

    int getMaxNameLength() {
        return NAME_MAX_LENGTH;
    }

    private static String getNameError() {
        return NAME_ERROR;
    }

    String getNameIllegalArgumentMessage() {
        return String.format(getNameError(), getMinNameLength(), getMaxNameLength());
    }

    public void addActivityHistory(String activity)
    {
        this.activityHistory.add(activity);
    }

    @Override
    public String showActivityHistory() {
        if(this.activityHistory.isEmpty()) return String.format(NO_MEMBER_ACTIVITY_HISTORY,this.name);
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Activity history of %s %s\n",getClassSimpleName(),this.name));
        this.activityHistory.forEach(history->{
            builder.append(history);
            builder.append("\n");
        });
        return builder.toString().trim();
    }

    @Override
    public String showWorkItems() {
        if(this.getWorkItems().isEmpty()) return String.format(USER_HAS_NO_WORK_ITEMS,this.name);
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Work items of %s %s\n",getClassSimpleName(),this.name));
        this.getWorkItems().forEach(item->{
            builder.append(item.toString());
            builder.append("\n==============");
        });
        return builder.toString().trim();
    }

    private String getClassSimpleName()
    {
        return this.getClass().getSimpleName().replace("Impl", "");
    }
}
