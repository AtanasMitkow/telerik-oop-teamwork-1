package com.company.models.team.contracts;

import java.util.List;

public interface Team {
    String getName();
    List<Member> getMembers();
    List<Board> getBoards();
    String showActivity();
    void addMember(Member member);
    void addBoard(Board board);
    String showBoards();
    String showMembers();
}
