package com.company.models.team.contracts;

import com.company.models.workItems.contracts.*;

import java.util.ArrayList;
import java.util.List;

public interface TeamUnit {
    String getName();
    List<WorkItem> getWorkItems();
    List<String> getActivityHistory();
    void addActivityHistory(String activity);
    void addBug(Bug bug);
    void addStory(Story story);
    void addFeedback(Feedback feedback);
    List<Bug> getBugs();
    List<Story> getStories();
    List<Feedback> getFeedbacks();
    String showActivityHistory();
    String showWorkItems();
    List<MemberWorkItem> getMemberWorkItems();
}
