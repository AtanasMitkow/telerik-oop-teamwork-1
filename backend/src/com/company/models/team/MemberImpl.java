package com.company.models.team;

import com.company.models.team.contracts.Member;

public class MemberImpl extends TeamUnitBase implements Member {
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 15;
    private static final String NAME_ERROR = "Name should be between %d and %d symbols.";

    public MemberImpl(String name) {
        super(name);
    }

    int getMinNameLength() {
        return  NAME_MIN_LENGTH;
    }

    int getMaxNameLength() {
        return  NAME_MAX_LENGTH;
    }

    protected String getNameIllegalArgumentMessage() {
        return String.format(NAME_ERROR, getMinNameLength(), getMaxNameLength());
    }


}
