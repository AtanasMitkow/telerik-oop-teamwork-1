package com.company.models.team;

import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {
    private static final String MEMBERS_ACTIVITY = "Members activity: ";
    private static final String MEMBER_ACTIVITY = "%s's activity:\n";
    private static final String SEPARATOR = "\n===============";
    private static final String BOARDS_ACTIVITY = "Boards activity:\n ";
    private static final String TEAM_BOARDS = "Team %s's boards:\n";
    private static final String TEAM_MEMBERS = "Team %s's members:\n";

    private String name;
    private List<Member> members;
    private List<Board> boards;

    public TeamImpl(String name) {
        setName(name);
        this.members = new ArrayList<>();
        this.boards = new ArrayList<>();
    }

    private void setName(String name) {
        this.name = name;
    }


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    public void addMember(Member member)
    {
        this.members.add(member);
    }

    public void addBoard(Board board) { this.boards.add(board); }

    @Override
    public String showActivity()
    {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format(MEMBER_ACTIVITY,this.name) );
        builder.append(MEMBERS_ACTIVITY);
        builder.append(SEPARATOR + "\n");

        members.forEach(member->
        {
            builder.append(member.showActivityHistory());
            builder.append(SEPARATOR);
        });

        builder.append(BOARDS_ACTIVITY);

        boards.forEach(board->{
            builder.append(board.showActivityHistory());
            builder.append(SEPARATOR);
        });

        return builder.toString().trim();
    }

    @Override
    public String showBoards()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(TEAM_BOARDS,name));
        boards.forEach(board->{
            builder.append(board.getName());
            builder.append("\n");
        });

        return builder.toString().trim();
    }

    @Override
    public String showMembers() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(TEAM_MEMBERS,name));
        members.forEach(member -> {
            builder.append(member.getName());
            builder.append("\n");
        });

        return builder.toString().trim();
    }
}
