package com.company.core.factories;

import com.company.enumerators.Priority;
import com.company.enumerators.Severity;
import com.company.enumerators.Size;
import com.company.enumerators.Status;
import com.company.models.team.BoardImpl;
import com.company.models.team.MemberImpl;
import com.company.models.team.TeamImpl;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.BugImpl;
import com.company.models.workItems.CommentImpl;
import com.company.models.workItems.FeedbackImpl;
import com.company.models.workItems.StoryImpl;
import com.company.models.workItems.contracts.Bug;
import com.company.models.workItems.contracts.Comment;
import com.company.models.workItems.contracts.Feedback;
import com.company.models.workItems.contracts.Story;

public class    FactoryImpl implements Factory {

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug createBug(String title, String description) {
        return new BugImpl(title,description);
    }

    @Override
    public Story createStory(String title, String description) {
        return new StoryImpl(title,description);
    }

    @Override
    public Feedback createFeedback(String title, String description ,int rating) {
        return new FeedbackImpl(title,description,rating);
    }

    @Override
    public Comment createComment(String author, String comment) {
        return new CommentImpl(author,comment);
    }
}
