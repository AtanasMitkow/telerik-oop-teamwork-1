package com.company.core.factories;

import com.company.enumerators.Priority;
import com.company.enumerators.Severity;
import com.company.enumerators.Size;
import com.company.enumerators.Status;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.Bug;
import com.company.models.workItems.contracts.Comment;
import com.company.models.workItems.contracts.Feedback;
import com.company.models.workItems.contracts.Story;

public interface Factory {
    Member createMember(String name);

    Team createTeam(String name);

    Board createBoard(String name);

    Bug createBug(String title, String description);

    Story createStory(String title, String description);

    Feedback createFeedback(String title, String description,int rating);

    Comment createComment(String author,String comment);
}
