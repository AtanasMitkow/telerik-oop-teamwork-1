package com.company.core;

public class CommandNameConstants {
    public static final String INVALID_COMMAND = "Invalid command name: %s!";
    public static final String HELP = "help";
    public static final String CREATE_MEMBER = "createmember" ;
    public static final String CREATE_TEAM = "createteam";
    public static final String CREATE_BOARD = "createboard";
    public static final String SHOW_MEMBERS = "showmembers";
    public static final String SHOW_ACTIVITY = "showactivity";
    public static final String SHOW_TEAMS = "showteams";
    public static final String SHOW_TEAM_MEMBERS = "showteammembers";
    public static final String SHOW_TEAM_ACTIVITY = "showteamactivity";
    public static final String SHOW_TEAM_BOARDS = "showteamboards";
    public static final String SHOW_BOARD_ACTIVITY = "showboardactivity";
    public static final String ADD_MEMBER = "addmember";
    public static final String ASSIGN_MEMBER = "assignmember";
    public static final String UNASSIGN_MEMBER = "unassingmember";
    public static final String ADD_COMMENT = "addcomment";
    public static final String CREATE_BUG = "createbug";
    public static final String CREATE_STORY = "createstory";
    public static final String CREATE_FEEDBACK = "createfeedback";
    public static final String CHANGE_STATUS = "changestatus";
    public static final String CHANGE_PRIORITY = "changepriority";
    public static final String CHANGE_SEVERITY = "changeseverity";
    public static final String CHANGE_RATING = "changerating";
    public static final String CHANGE_SIZE = "changesize";
    public static final String LIST_ALL_WORK_ITEMS = "listallworkitems";
    public static final String LIST_ALL_BY_TYPE = "listallbytype";
    public static final String LIST_ALL_BY = "listallby";
    public static final String SORT_ITEMS_BY_TITLE = "sortitemsbytitle";
    public static final String SORT_ITEMS_BY_SIZE = "sortitemsbysize";
    public static final String SORT_ITEMS_BY_SEVERITY = "sortitemsbyseverity";
    public static final String SORT_ITEMS_BY_RATING = "sortitemsbyrating";
    public static final String SORT_ITEMS_BY_PRIORITY = "sortitemsbypriority";
}
