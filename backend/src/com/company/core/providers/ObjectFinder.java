package com.company.core.providers;

import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.MemberWorkItem;
import com.company.models.workItems.contracts.WorkItem;

import java.util.List;

public class ObjectFinder implements Finder {
    private static final String INVALID_TEAM_NAME = "Team with name %s does not exist!";
    private static final String BOARD_EXISTANCE_ERROR = "Board doest not exist!";

    private Engine engine;

    public ObjectFinder(Engine engine)
    {
        this.engine = engine;
    }

    @Override
    public Member findMember(String name) {
        return engine.getMembers().stream()
                .filter(member -> member.getName().toUpperCase().equals(name.toUpperCase()))
                .findAny()
                .orElse(null);
    }

    @Override
    public Board findBoardInTeam(String boardName, String teamName) {
        Team team = findTeam(teamName);
        if(team == null) throw new IllegalArgumentException(String.format(INVALID_TEAM_NAME,teamName));

        return team.getBoards().stream()
                .filter(board -> board.getName().equals(boardName))
                .findAny()
                .orElse(null);
    }

    @Override
    public WorkItem findWorkItemInBoard(Board board, String workItemName) {
        if(board == null) throw new IllegalArgumentException(BOARD_EXISTANCE_ERROR);

        return board.getWorkItems().stream()
                .filter(workItem -> workItem.getTitle().equals(workItemName))
                .findAny()
                .orElse(null);
    }

    @Override
    public Team findTeam(String name) {
        return engine.getTeams().stream()
                .filter(team -> team.getName().equals(name))
                .findAny()
                .orElse(null);
    }

    public Member findMemberInTeam(String memberName, String teamName) {
        Team team = findTeam(teamName);
        if(team == null) throw new IllegalArgumentException(String.format(INVALID_TEAM_NAME,teamName));

        return team.getMembers().stream()
                .filter(member -> member.getName().toUpperCase().equals(memberName.toUpperCase()))
                .findAny()
                .orElse(null);
    }

    @Override
    public MemberWorkItem findAssignableItem(Board board,String itemName) {
        if(board == null) throw new IllegalArgumentException(BOARD_EXISTANCE_ERROR);
        List<MemberWorkItem> items = board.getMemberWorkItems();

        return items.stream()
                .filter(item->item.getTitle().equals(itemName))
                .findAny()
                .orElse(null);
    }

    @Override
    public <E extends WorkItem> E findItemInCollection(List<E> list, String itemName) {

        return list.stream().filter(element->element.getTitle().equals(itemName))
                .findAny()
                .orElse(null);

    }


}
