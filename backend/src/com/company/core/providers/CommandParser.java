package com.company.core.providers;

import com.company.commands.adding.AddCommentCommand;
import com.company.commands.adding.AddMemberToTeamCommand;
import com.company.commands.adding.AssignCommand;
import com.company.commands.adding.UnassignCommand;
import com.company.commands.changing.*;
import com.company.commands.contracts.Command;
import com.company.commands.creation.CreateBoardCommand;
import com.company.commands.creation.CreateMemberCommand;
import com.company.commands.creation.CreateTeamCommand;
import com.company.commands.help.HelpCommand;
import com.company.commands.listing.*;
import com.company.commands.listing.listingwithoptions.ListAllByCommand;
import com.company.commands.listing.listingwithoptions.ListAllByTypeCommand;
import com.company.commands.listing.listingwithoptions.ListAllCommand;
import com.company.commands.sorting.*;
import com.company.commands.workItemCreation.CreateBugCommand;
import com.company.commands.workItemCreation.CreateFeedbackCommand;
import com.company.commands.workItemCreation.CreateStoryCommand;
import com.company.core.CommandNameConstants;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Parser;
import com.company.core.factories.Factory;

import java.util.ArrayList;
import java.util.List;



public class CommandParser implements Parser {
    private final Factory factory;
    private final Engine engine;

    public CommandParser(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split(" ")[0];
        List<String> parameters = parseParameters(fullCommand);
        return executeCommand(commandName,parameters);
    }

    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split(" ");
        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < commandParts.length; i++) {
            parameters.add(commandParts[i]);
        }
        return parameters;
    }

    private Command executeCommand(String commandName, List<String> parameters) {
        switch (commandName.toLowerCase()) {
            case CommandNameConstants.HELP:
                return new HelpCommand(factory,engine);

            //BASE CREATION
            case CommandNameConstants.CREATE_MEMBER:
                return new CreateMemberCommand(factory,engine);

            case CommandNameConstants.CREATE_TEAM:
                return new CreateTeamCommand(factory,engine);

            case CommandNameConstants.CREATE_BOARD:
                return new CreateBoardCommand(factory,engine);

            //SHOWING
            case CommandNameConstants.SHOW_MEMBERS:
                return new ShowMembersCommand(factory,engine);

            case CommandNameConstants.SHOW_ACTIVITY:
                return new ShowActivityOfMemberCommand(factory,engine);

            case CommandNameConstants.SHOW_TEAMS:
                return new ShowTeamsCommand(factory,engine);

            case CommandNameConstants.SHOW_TEAM_MEMBERS:
                return new ShowTeamMembersCommand(factory,engine);

            case CommandNameConstants.SHOW_TEAM_ACTIVITY:
                return new ShowTeamActivityCommand(factory,engine);

            case CommandNameConstants.SHOW_TEAM_BOARDS:
                return new ShowTeamBoardsCommand(factory,engine);

            case CommandNameConstants.SHOW_BOARD_ACTIVITY:
                return new ShowBoardActivityCommand(factory,engine);

            //Adding
            case CommandNameConstants.ADD_MEMBER:
                return new AddMemberToTeamCommand(factory,engine);

            case CommandNameConstants.ASSIGN_MEMBER:
                return new AssignCommand(factory,engine);

            case CommandNameConstants.UNASSIGN_MEMBER:
                return new UnassignCommand(factory,engine);

            case CommandNameConstants.ADD_COMMENT:
                return new AddCommentCommand(factory,engine);


            //WorkItem Creation
            case CommandNameConstants.CREATE_BUG:
            {
                return new CreateBugCommand(factory,engine);
            }
            case CommandNameConstants.CREATE_STORY:
            {
                return new CreateStoryCommand(factory,engine);
            }

            case CommandNameConstants.CREATE_FEEDBACK:
            {
                return new CreateFeedbackCommand(factory,engine);
            }


            //Change commands
            case CommandNameConstants.CHANGE_STATUS:
                return new ChangeStatusCommand(factory,engine);

            case CommandNameConstants.CHANGE_PRIORITY:
                return new ChangePriorityCommand(factory,engine);

            case CommandNameConstants.CHANGE_SEVERITY:
                return new ChangeSeverityCommand(factory,engine);

            case CommandNameConstants.CHANGE_RATING:
                return new ChangeRatingCommand(factory,engine);

            case CommandNameConstants.CHANGE_SIZE:
                return new ChangeSizeCommand(factory,engine);

            //Show commands with options

            case CommandNameConstants.LIST_ALL_WORK_ITEMS:
                return new ListAllCommand(factory,engine);

            case CommandNameConstants.LIST_ALL_BY_TYPE:
                return new ListAllByTypeCommand(factory,engine);

            case CommandNameConstants.LIST_ALL_BY:
                return new ListAllByCommand(factory,engine);

            //Sort command
            case CommandNameConstants.SORT_ITEMS_BY_TITLE:
                return new SortByTitleCommand(factory,engine);

            case CommandNameConstants.SORT_ITEMS_BY_SIZE:
                return new SortBySizeCommand(factory,engine);

            case CommandNameConstants.SORT_ITEMS_BY_SEVERITY:
                return new SortBySeverityCommand(factory,engine);

            case CommandNameConstants.SORT_ITEMS_BY_RATING:
                return new SortByRatingCommand(factory,engine);

            case CommandNameConstants.SORT_ITEMS_BY_PRIORITY:
                return new SortByPriorityCommand(factory,engine);

        }
        throw new IllegalArgumentException(String.format(CommandNameConstants.INVALID_COMMAND, commandName));
    }
}
