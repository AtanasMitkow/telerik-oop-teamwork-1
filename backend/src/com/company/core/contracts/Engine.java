package com.company.core.contracts;

import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.*;

import java.util.List;

public interface Engine {
    void start();

    List<Member> getMembers();

    List<Team> getTeams();

    List<Board> getBoards();

    List<WorkItem> getAllWorkItems();

    public List<Bug> getBugs();

    public List<Story> getStories();

    public List<Feedback> getFeedbacks();

    public List<MemberWorkItem> getMemberWorkItems();

    <T> String workItemListToString(List<T> items);

}
