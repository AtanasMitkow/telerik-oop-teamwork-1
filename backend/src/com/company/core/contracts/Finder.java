package com.company.core.contracts;

import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.WorkItemBase;
import com.company.models.workItems.contracts.MemberWorkItem;
import com.company.models.workItems.contracts.WorkItem;

import java.util.List;

public interface Finder {
    Member findMember(String name);
    Board findBoardInTeam(String boardName,String teamName);
    WorkItem findWorkItemInBoard(Board board,String workItemName);
    Team findTeam(String name);
    Member findMemberInTeam(String memberName,String teamName);
    MemberWorkItem findAssignableItem(Board board, String itemName);
    <E extends WorkItem> E findItemInCollection(List<E> list,String itemName);
}
