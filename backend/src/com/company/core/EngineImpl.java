package com.company.core;


import com.company.commands.contracts.Command;
import com.company.core.contracts.*;
import com.company.core.factories.Factory;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ConsoleReader;
import com.company.core.providers.ConsoleWriter;
import com.company.core.providers.ObjectFinder;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.*;

import java.util.ArrayList;
import java.util.List;

public class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private Reader reader;
    private Writer writer;
    private Parser parser;
    private Finder finder;
    private final List<Member> members;
    private final List<Team> teams;
    private final List<Board> boards;
    private final List<Bug> bugs;
    private final List<Feedback> feedbacks;
    private final List<Story> stories;

    public EngineImpl(Factory factory) {
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);
        finder = new ObjectFinder(this);

        members = new ArrayList<>();
        teams = new ArrayList<>();
        boards = new ArrayList<>();
        bugs = new ArrayList<>();
        feedbacks = new ArrayList<>();
        stories = new ArrayList<>();
    }

    public List<WorkItem> getAllWorkItems()
    {
        List<WorkItem> workItems = new ArrayList<>(bugs);
        workItems.addAll(stories);
        workItems.addAll(feedbacks);
        return workItems;
    }

    @Override
    public List<Bug> getBugs() {
        return bugs;
    }

    @Override
    public List<Story> getStories() {
        return stories;
    }

    @Override
    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    @Override
    public List<MemberWorkItem> getMemberWorkItems()
    {
        List<MemberWorkItem> item = new ArrayList<>(bugs);
        item.addAll(stories);
        return item;
    }

    @Override
    public <T> String workItemListToString(List<T> items) {
        StringBuilder builder = new StringBuilder();
        items.forEach(item->builder.append(item.toString()));
        return builder.toString().trim();
    }

    @Override
    public List<Member> getMembers() {
        return members;
    }

    @Override
    public List<Team> getTeams() {
        return teams;
    }

    @Override
    public List<Board> getBoards() {
        return boards;
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
                //writer.writeLine("####################");
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Commands cannot be null or empty.");
        }
        List<String> parameters = parser.parseParameters(commandAsString);
        Command command = parser.parseCommand(commandAsString);
        String commandResult = command.execute(parameters,finder);
        writer.writeLine(commandResult);
        //writer.writeLine("####################");
    }
}
