package com.company.enumerators;

public enum Size {
    Large,
    Medium,
    Small,

    //default
    Unset
}
