package com.company.enumerators;

public enum Status {
    //Bug
    Active,
    Fixed,

    //Story
    NotDone,
    InProgress,

    //Feedback
    New,
    Unscheduled,
    Scheduled,
    Done,

    Unset;

    @Override
    public String toString() {
        switch (this) {
            case Unset:
                return "Status is not currently set";
            case Active:
                return "Active";
            case Fixed:
                return "Fixed";
            case NotDone:
                return "NotDone";
            case InProgress:
                return "InProgress";
            case Done:
                return "Done";
            case New:
                return "New";
            case Unscheduled:
                return "Unscheduled";
            case Scheduled:
                return "Scheduled";

            //finish it
            default:
                throw new IllegalArgumentException();
        }
    }
}
