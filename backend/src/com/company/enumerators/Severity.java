package com.company.enumerators;

public enum Severity {
    Critical,
    Major,
    Minor,
    Unset
}
