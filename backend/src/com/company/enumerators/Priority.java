package com.company.enumerators;

public enum Priority {
    High,
    Medium,
    Low,
    Unset
}
