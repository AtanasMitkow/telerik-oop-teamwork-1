package com.companytests.models;

public class TestConstants {
    public static final String TITLE_SAMPLE = "TestTitleName";
    public static final String TITLE_MORE_THAN_MAX = "123456789";
    public static final String TITLE_LESS_THAN_MIN = "50test50test50test50test50test50test50test50test50test";
    public static final String DESCRIPTION_SAMPLE = "DescriptionTester";
    public static final String DESCRIPTION_MORE_THAN_MAX = "500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500test500";
    public static final String DESCRIPTION_LESS_THAN_MIN = "123456789";
    public static final String NAME_SAMPLE = "TestName";
    public static final String NAME_MORE_THAN_MAX = "TestNameLongerThanMaxLenght";
    public static final String NAME_LESS_THAN_MIN = "Lowe";
//    public static final String
//    public static final String
//    public static final String
//    public static final String
//    public static final String
//    public static final String
}
