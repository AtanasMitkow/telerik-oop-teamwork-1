package com.companytests.models.workItemsAndChanging;

import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.enumerators.Status;
import com.company.models.workItems.WorkItemBase;
import com.company.models.workItems.contracts.Feedback;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CreateFeedBackAndChangingTests {
    private Factory factory;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
    }

    @Test
    public void returnInstanceOfTypeFeedBack() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE, 20);
        Assert.assertTrue(feedback instanceof WorkItemBase);
    }

    @Test(expected = IllegalArgumentException.class)
    public void longerThanMaxTitleLength() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_MORE_THAN_MAX, TestConstants.DESCRIPTION_MORE_THAN_MAX, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lowerThanMinTitleLength() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_LESS_THAN_MIN, TestConstants.DESCRIPTION_LESS_THAN_MIN, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lowerThanMinRating() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_LESS_THAN_MIN, TestConstants.DESCRIPTION_LESS_THAN_MIN, -34);
    }

    @Test(expected = IllegalArgumentException.class)
    public void biggerThanMaxRating() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_MORE_THAN_MAX, TestConstants.DESCRIPTION_MORE_THAN_MAX, 120);
    }

    @Test
    public void correctFeedBackTitleAndDesc() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE, 20);
    }

    @Test
    public void changeStatusTest() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE, 20);
        Assert.assertTrue(feedback.getStatus().equals(Status.Unset));
    }

    @Test
    public void changeStatusTest2() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE, 20);
        feedback.changeStatus(Status.Scheduled);
        Assert.assertTrue(feedback.getStatus().equals(Status.Scheduled));
    }

    @Test
    public void changeRatingTest() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE, 20);
        Assert.assertTrue(feedback.getRating() == 20);
    }

    @Test
    public void changeRatingTest2() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE, 20);
        feedback.changeRating(50);
        Assert.assertTrue(feedback.getRating() == 50);
    }
}
