package com.companytests.models.workItemsAndChanging;

import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.enumerators.Priority;
import com.company.enumerators.Severity;
import com.company.enumerators.Status;
import com.company.models.workItems.WorkItemBase;
import com.company.models.workItems.contracts.Bug;

import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CreateBugAndChangingTests {
    private Factory factory;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
    }

    @Test
    public void returnInstanceOfTypeBug() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(bug instanceof WorkItemBase);
    }

    @Test(expected = IllegalArgumentException.class)
    public void longerThanMaxTitleLength() {
        Bug bug = factory.createBug(TestConstants.TITLE_MORE_THAN_MAX, TestConstants.DESCRIPTION_MORE_THAN_MAX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lowerThanMinTitleLength() {
        Bug bug = factory.createBug(TestConstants.TITLE_LESS_THAN_MIN, TestConstants.DESCRIPTION_LESS_THAN_MIN);
    }

    @Test
    public void correctBugTitleAndDesc() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
    }

    @Test
    public void changeStatusTest() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(bug.getStatus().equals(Status.Unset));
    }

    @Test
    public void changeStatusTest2() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        bug.changeStatus(Status.Fixed);
        Assert.assertTrue(bug.getStatus().equals(Status.Fixed));
    }

    @Test
    public void changeSeverityTest() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(bug.getSeverity().equals(Severity.Unset));
    }

    @Test
    public void changeSeverityTest2() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        bug.changeSeverity(Severity.Critical);
        Assert.assertTrue(bug.getSeverity().equals(Severity.Critical));
    }

    @Test
    public void changePriorityTest() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(bug.getPriority().equals(Priority.Unset));
    }

    @Test
    public void changePriorityTest2() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        bug.changePriority(Priority.High);
        Assert.assertTrue(bug.getPriority().equals(Priority.High));
    }
}
