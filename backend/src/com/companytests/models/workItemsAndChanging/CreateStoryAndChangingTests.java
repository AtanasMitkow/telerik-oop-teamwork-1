package com.companytests.models.workItemsAndChanging;

import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.enumerators.Priority;
import com.company.enumerators.Size;
import com.company.enumerators.Status;
import com.company.models.workItems.WorkItemBase;
import com.company.models.workItems.contracts.Story;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CreateStoryAndChangingTests {
    private Factory factory;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
    }

    @Test
    public void returnInstanceOfTypeStory() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(story instanceof WorkItemBase);
    }

    @Test(expected = IllegalArgumentException.class)
    public void longerThanMaxTitleLength() {
        Story story = factory.createStory(TestConstants.TITLE_MORE_THAN_MAX, TestConstants.DESCRIPTION_MORE_THAN_MAX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lowerThanMinTitleLength() {
        Story story = factory.createStory(TestConstants.TITLE_LESS_THAN_MIN, TestConstants.DESCRIPTION_LESS_THAN_MIN);
    }

    @Test
    public void correctStoryTitleAndDesc() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertEquals(story.getTitle(), TestConstants.TITLE_SAMPLE);
        Assert.assertEquals(story.getDescription(), TestConstants.DESCRIPTION_SAMPLE);
    }

    @Test
    public void changeStatusTest() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(story.getStatus().equals(Status.Unset));
    }

    @Test
    public void changeStatusTest2() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        story.changeStatus(Status.NotDone);
        Assert.assertTrue(story.getStatus().equals(Status.NotDone));
    }

    @Test
    public void changeSizeTest() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(story.getSize().equals(Size.Unset));
    }

    @Test
    public void changeSizeTest2() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        story.changeSize(Size.Large);
        Assert.assertTrue(story.getSize().equals(Size.Large));
    }

    @Test
    public void changePriorityTest() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Assert.assertTrue(story.getPriority().equals(Priority.Unset));
    }

    @Test
    public void changePriorityTest2() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        story.changePriority(Priority.High);
        Assert.assertTrue(story.getPriority().equals(Priority.High));
    }
}
