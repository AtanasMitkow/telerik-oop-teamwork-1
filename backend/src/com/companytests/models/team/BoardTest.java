package com.companytests.models.team;

import com.company.commands.creation.CreateBoardCommand;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.models.team.TeamImpl;
import com.company.models.team.TeamUnitBase;
import com.company.models.team.contracts.Board;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class BoardTest {
    private Factory factory;
    private Engine engine;
    @Before
    public void runBeforeTests() {
         factory = new FactoryImpl();
         engine = new EngineImpl(factory);
    }

    @Test
   public void returnInstanceOfTypeBoard() {
        Board board = factory.createBoard(TestConstants.NAME_SAMPLE);
        Assert.assertTrue(board instanceof TeamUnitBase);
    }

    @Test(expected = IllegalArgumentException.class)
    public void longerThanMaxNameLength() {
        Board board = factory.createBoard(TestConstants.NAME_MORE_THAN_MAX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lowerThanMinNameLength() {
        Board board = factory.createBoard(TestConstants.NAME_LESS_THAN_MIN);
    }

    @Test
    public void correctBoardName() {
        Board board = factory.createBoard(TestConstants.NAME_SAMPLE);
        Assert.assertEquals(board.getName(), TestConstants.NAME_SAMPLE);
    }
}
