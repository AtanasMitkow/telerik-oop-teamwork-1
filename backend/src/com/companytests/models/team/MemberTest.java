package com.companytests.models.team;

import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.models.team.TeamUnitBase;
import com.company.models.team.contracts.Member;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MemberTest {
    private Factory factory;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
    }

    @Test
    public void returnInstanceOfTypeMember() {
        Member member = factory.createMember(TestConstants.NAME_SAMPLE);
        Assert.assertTrue(member instanceof TeamUnitBase);
    }

    @Test(expected = IllegalArgumentException.class)
    public void longerThanMaxNameLength() {
        Member member = factory.createMember(TestConstants.NAME_MORE_THAN_MAX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lowerThanMinNameLength() {
        Member member = factory.createMember(TestConstants.NAME_LESS_THAN_MIN);
    }

    @Test
    public void correctMemberName() {
        Member member = factory.createMember(TestConstants.NAME_SAMPLE);
    }
}
