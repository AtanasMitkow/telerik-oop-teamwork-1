package com.companytests.models.team;

import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.models.team.TeamImpl;
import com.company.models.team.TeamUnitBase;
import com.company.models.team.contracts.Team;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TeamTest {
    private Factory factory;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
    }

    @Test
    public void returnInstanceOfTypeTeam() {
        Team team = factory.createTeam(TestConstants.NAME_SAMPLE);
        Assert.assertTrue(team instanceof TeamImpl);
    }

    @Test
    public void correctTeamName() {
        Team team = factory.createTeam(TestConstants.NAME_SAMPLE);
    }
}
