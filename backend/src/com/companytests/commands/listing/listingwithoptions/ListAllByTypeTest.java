package com.companytests.commands.listing.listingwithoptions;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.workItems.contracts.Bug;
import com.company.models.workItems.contracts.Feedback;
import com.company.models.workItems.contracts.Story;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ListAllByTypeTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }
    @Test
    public void createWorkItemsAndListBugs() {
        Bug bug = factory.createBug("BugTitleTester", "BugDescriptionTester");
        engine.getBugs().add(bug);
        String bugid = bug.getID();
        Story story = factory.createStory("StoryTitleTester", "StoryDescriptionTester");
        engine.getStories().add(story);
        String storyid = story.getID();
        Feedback feedback = factory.createFeedback("FeedBackTitleTester", "FeedBackDescroptionTester", 50);
        engine.getFeedbacks().add(feedback);
        String feedbackid = feedback.getID();
        Bug bug2 = factory.createBug("BugTitleTester2", "BugDescriptionTester2");
        engine.getBugs().add(bug2);
        String bug2id = bug2.getID();
        List<String> parameters = commandParser.parseParameters("listallbytype bug");
        Command testCommand = commandParser.parseCommand("listallbytype bug");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals("ID: " + bugid + ",\n" +
                "Title: BugTitleTester,\n" +
                "Description: BugDescriptionTester\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not setID: " + bug2id + ",\n" +
                "Title: BugTitleTester2,\n" +
                "Description: BugDescriptionTester2\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not set", result);
    }
}
