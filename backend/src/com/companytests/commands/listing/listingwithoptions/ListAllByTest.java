package com.companytests.commands.listing.listingwithoptions;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.enumerators.Status;
import com.company.models.team.contracts.Member;
import com.company.models.workItems.contracts.Bug;
import com.company.models.workItems.contracts.Feedback;
import com.company.models.workItems.contracts.Story;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ListAllByTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }
    @Test
    public void createWorkItemsAndListAllByStatus() {
        Bug bug = factory.createBug("BugTitleTester", "BugDescriptionTester");
        engine.getBugs().add(bug);
        String bugid = bug.getID();
        bug.changeStatus(Status.Active);
        Story story = factory.createStory("StoryTitleTester", "StoryDescriptionTester");
        engine.getStories().add(story);
        String storyid = story.getID();
        story.changeStatus(Status.InProgress);
        Bug bug2 = factory.createBug("BugTitleTester2", "BugDescriptionTester2");
        engine.getBugs().add(bug2);
        String bug2id = bug2.getID();
        bug2.changeStatus(Status.Fixed);
        Feedback feedback = factory.createFeedback("FeedBackTitleTester", "FeedBackDescroptionTester", 50);
        engine.getFeedbacks().add(feedback);
        String feedbackid = feedback.getID();
        feedback.changeStatus(Status.Scheduled);
        Bug bug3 = factory.createBug("BugTitleTester3", "BugDescriptionTester3");
        engine.getBugs().add(bug3);
        String bug3id = bug3.getID();
        bug3.changeStatus(Status.Fixed);
        List<String> parameters = commandParser.parseParameters("listallby status Fixed");
        Command testCommand = commandParser.parseCommand("listallby status Fixed");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals("ID: " + bug2id + ",\n" +
                "Title: BugTitleTester2,\n" +
                "Description: BugDescriptionTester2\n" +
                "Status: Fixed\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not setID: " + bug3id + ",\n" +
                "Title: BugTitleTester3,\n" +
                "Description: BugDescriptionTester3\n" +
                "Status: Fixed\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not set", result);
    }

    @Test
    public void createWorkItemsAndListAllByAssignee() {
        Member member = factory.createMember("Atanas");
        Bug bug = factory.createBug("BugTitleTester", "BugDescriptionTester");
        engine.getBugs().add(bug);
        String bugid = bug.getID();
        bug.changeStatus(Status.Active);
        bug.assignMember(member);
        Story story = factory.createStory("StoryTitleTester", "StoryDescriptionTester");
        engine.getStories().add(story);
        String storyid = story.getID();
        story.changeStatus(Status.InProgress);
        story.assignMember(member);
        Bug bug2 = factory.createBug("BugTitleTester2", "BugDescriptionTester2");
        engine.getBugs().add(bug2);
        String bug2id = bug2.getID();
        bug2.changeStatus(Status.Fixed);
        Feedback feedback = factory.createFeedback("FeedBackTitleTester", "FeedBackDescroptionTester", 50);
        engine.getFeedbacks().add(feedback);
        String feedbackid = feedback.getID();
        feedback.changeStatus(Status.Scheduled);
        Bug bug3 = factory.createBug("BugTitleTester3", "BugDescriptionTester3");
        engine.getBugs().add(bug3);
        String bug3id = bug3.getID();
        bug3.changeStatus(Status.Fixed);
        List<String> parameters = commandParser.parseParameters("listallby assignee Atanas");
        Command testCommand = commandParser.parseCommand("listallby assignee Atanas");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals("ID: " + bugid +  ",\n" +
                "Title: BugTitleTester,\n" +
                "Description: BugDescriptionTester\n" +
                "Status: Active\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: Atanas\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not setID: " + storyid + ",\n" +
                "Title: StoryTitleTester,\n" +
                "Description: StoryDescriptionTester\n" +
                "Status: InProgress\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: Atanas\n" +
                "Priority: Priority is not set\n" +
                "Size: Size is not set", result);
    }

    @Test
    public void createWorkItemsAndListAllByStatusAndAssignee() {
        Member member = factory.createMember("Atanas");
        Bug bug = factory.createBug("BugTitleTester", "BugDescriptionTester");
        engine.getBugs().add(bug);
        String bugid = bug.getID();
        bug.changeStatus(Status.Active);
        bug.assignMember(member);
        Story story = factory.createStory("StoryTitleTester", "StoryDescriptionTester");
        engine.getStories().add(story);
        String storyid = story.getID();
        story.changeStatus(Status.InProgress);
        story.assignMember(member);
        Bug bug2 = factory.createBug("BugTitleTester2", "BugDescriptionTester2");
        engine.getBugs().add(bug2);
        String bug2id = bug2.getID();
        bug2.changeStatus(Status.Fixed);
        Feedback feedback = factory.createFeedback("FeedBackTitleTester", "FeedBackDescroptionTester", 50);
        engine.getFeedbacks().add(feedback);
        String feedbackid = feedback.getID();
        feedback.changeStatus(Status.Scheduled);
        Bug bug3 = factory.createBug("BugTitleTester3", "BugDescriptionTester3");
        engine.getBugs().add(bug3);
        String bug3id = bug3.getID();
        bug3.changeStatus(Status.Fixed);
        bug3.assignMember(member);
        List<String> parameters = commandParser.parseParameters("listallby status Fixed assignee Atanas");
        Command testCommand = commandParser.parseCommand("listallby status Fixed assignee Atanas");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals("ID: " + bug3id + ",\n" +
                "Title: BugTitleTester3,\n" +
                "Description: BugDescriptionTester3\n" +
                "Status: Fixed\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: Atanas\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not set", result);
    }
}
