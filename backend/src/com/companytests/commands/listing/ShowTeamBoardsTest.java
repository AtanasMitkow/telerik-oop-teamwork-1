package com.companytests.commands.listing;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowTeamBoardsTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test
    public void createAndShowOneTeamBoard() {
        Team team = factory.createTeam("TeamName");
        Board board = factory.createBoard("BoardName");
        engine.getTeams().add(team);
        List<String> parameters = commandParser.parseParameters("showteamboards TeamName");
        team.addBoard(board);
        Command testCommand = commandParser.parseCommand("showteamboards TeamName");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "Team TeamName's boards:\n" +
                "BoardName");
    }

    @Test
    public void createAndShowMultipleTeamMembers() {
        Team team = factory.createTeam("TeamName");
        Board board1 = factory.createBoard("BoardName1");
        Board board2 = factory.createBoard("BoardName2");
        Board board3 = factory.createBoard("BoardName3");
        engine.getTeams().add(team);
        List<String> parameters = commandParser.parseParameters("showteamboards TeamName");
        team.addBoard(board1);
        team.addBoard(board2);
        team.addBoard(board3);
        Command testCommand = commandParser.parseCommand("showteamboards TeamName");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "Team TeamName's boards:\n" +
                "BoardName1\n" +
                "BoardName2\n" +
                "BoardName3");
    }
}
