package com.companytests.commands.listing;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowMembersTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test
    public void createAndShowOneTeam() {
        Member member = factory.createMember("MemberName");
        List<Member> members = engine.getMembers();
        List<String> parameters = commandParser.parseParameters("showmembers");
        members.add(member);
        Command testCommand = commandParser.parseCommand("showmembers");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "All members:\n" +
                "MemberName");
    }

    @Test
    public void createAndShowMultipleTeams() {
        Member member1 = factory.createMember("MemberName1");
        Member member2 = factory.createMember("MemberName2");
        Member member3 = factory.createMember("MemberName3");
        List<Member> members = engine.getMembers();
        List<String> parameters = commandParser.parseParameters("showmembers");
        members.add(member1);
        members.add(member2);
        members.add(member3);
        Command testCommand = commandParser.parseCommand("showmembers");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "All members:\n" +
                "MemberName1\n" +
                "MemberName2\n" +
                "MemberName3");
    }
}
