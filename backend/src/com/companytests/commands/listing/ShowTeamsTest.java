package com.companytests.commands.listing;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.team.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowTeamsTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test
    public void createAndShowOneTeam() {
        Team team1 = factory.createTeam("TestName");
        List<Team> teams = engine.getTeams();
        List<String> parameters = commandParser.parseParameters("showteams");
        teams.add(team1);
        Command testCommand = commandParser.parseCommand("showteams");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "All teams:\n" +
                "TestName");
    }

    @Test
    public void createAndShowMultipleTeams() {
        Team team1 = factory.createTeam("TestName");
        Team team2 = factory.createTeam("TestName2");
        Team team3 = factory.createTeam("TestName3");
        List<Team> teams = engine.getTeams();
        List<String> parameters = commandParser.parseParameters("showteams");
        teams.add(team1);
        teams.add(team2);
        teams.add(team3);
        Command testCommand = commandParser.parseCommand("showteams");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "All teams:\n" +
                "TestName\n" +
                "TestName2\n" +
                "TestName3");
    }
}
