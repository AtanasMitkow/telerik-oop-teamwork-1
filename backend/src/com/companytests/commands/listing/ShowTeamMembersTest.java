package com.companytests.commands.listing;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowTeamMembersTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test
    public void createAndShowOneTeamMember() {
        Team team = factory.createTeam("TeamName");
        Member member = factory.createMember("MemberName");
        engine.getTeams().add(team);
        List<String> parameters = commandParser.parseParameters("showteammembers TeamName");
        team.addMember(member);
        Command testCommand = commandParser.parseCommand("showteammembers TeamName");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "Team TeamName's members:\n" +
                "MemberName");
    }

    @Test
    public void createAndShowMultipleTeamMembers() {
        Team team = factory.createTeam("TeamName");
        Member member1 = factory.createMember("MemberName1");
        Member member2 = factory.createMember("MemberName2");
        Member member3 = factory.createMember("MemberName3");
        engine.getTeams().add(team);
        List<String> parameters = commandParser.parseParameters("showteammembers TeamName");
        team.addMember(member1);
        team.addMember(member2);
        team.addMember(member3);
        Command testCommand = commandParser.parseCommand("showteammembers TeamName");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals(result, "Team TeamName's members:\n" +
                "MemberName1\n" +
                "MemberName2\n" +
                "MemberName3");
    }
}
