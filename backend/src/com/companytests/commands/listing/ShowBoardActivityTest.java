package com.companytests.commands.listing;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.Bug;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ShowBoardActivityTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test
    public void createAndShowActivityOfMember() {
        Member member = factory.createMember("MemberName");
        engine.getMembers().add(member);
        member.addActivityHistory("Member created");
        Team team = factory.createTeam("TeamName");
        engine.getTeams().add(team);
        team.addMember(member);
        member.addActivityHistory("MemberName was added to team TeamName.");
        Board board = factory.createBoard("BoardName");
        engine.getBoards().add(board);
        board.addActivityHistory("Board created");
        team.addBoard(board);
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        board.addBug(bug);
        board.addActivityHistory(String.format("Bug with name %s was created.", bug.getTitle()));
        bug.assignMember(member);
        member.addActivityHistory("MemberName assigned to work item TestTitleName");
        List<String> parameters = commandParser.parseParameters("showboardactivity TeamName BoardName");
        Command testCommand = commandParser.parseCommand("showboardactivity TeamName BoardName");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals( "Activity history of Board BoardName\n" +
                "Board created\n" +
                "Bug with name TestTitleName was created.", result);
    }
}
