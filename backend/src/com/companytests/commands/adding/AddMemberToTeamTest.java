package com.companytests.commands.adding;

import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AddMemberToTeamTest {
    private Factory factory;
    private Engine engine;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
    }


    @Test
    public void addMemberToTeamWhenMemberIsValid() {
        Member member = factory.createMember("TestName");
        Team team = factory.createTeam("TestName");
        Assert.assertEquals(0, team.getMembers().size());
        team.addMember(member);
        Assert.assertEquals(1, team.getMembers().size());
    }
}
