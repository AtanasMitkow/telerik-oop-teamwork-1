package com.companytests.commands.adding;

import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.models.team.contracts.Board;
import com.company.models.workItems.contracts.Bug;
import com.company.models.workItems.contracts.Feedback;
import com.company.models.workItems.contracts.Story;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AddWorkItemTest {
    private Factory factory;
    private Engine engine;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void addWorkItemWhenItemIsValidBug() {
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Board board = factory.createBoard("TestName");
        Assert.assertEquals(0,board.getBugs().size());
        board.addBug(bug);
        Assert.assertEquals(1, board.getBugs().size());
    }

    @Test
    public void addWorkItemWhenItemIsValidFeedBack() {
        Feedback feedback = factory.createFeedback(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE, 20);
        Board board = factory.createBoard("TestName");
        Assert.assertEquals(0, board.getFeedbacks().size());
        board.addFeedback(feedback);
        Assert.assertEquals(1, board.getFeedbacks().size());
    }

    @Test
    public void addWorkItemWhenItemIsValidStory() {
        Story story = factory.createStory(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        Board board = factory.createBoard("TestName");
        Assert.assertEquals(0, board.getStories().size());
        board.addStory(story);
        Assert.assertEquals(1, board.getStories().size());
    }
}
