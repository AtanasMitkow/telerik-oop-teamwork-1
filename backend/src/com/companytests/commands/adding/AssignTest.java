package com.companytests.commands.adding;

import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Member;
import com.company.models.team.contracts.Team;
import com.company.models.workItems.contracts.Bug;
import com.companytests.models.TestConstants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AssignTest {
    private Factory factory;
    private Engine engine;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void assignProperMemberInTeamBoard() {
        Team team = factory.createTeam("TeamName");
        Board board = factory.createBoard("BoardName");
        Member member = factory.createMember("MemberName");
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        team.addBoard(board);
        board.addBug(bug);
        bug.assignMember(member);
        Assert.assertEquals("MemberName", bug.getAssignee().getName());
    }

    @Test
    public void assignNewProperMemberInTeamBoard() {
        Team team = factory.createTeam("TeamName");
        Board board = factory.createBoard("BoardName");
        Member member = factory.createMember("MemberName");
        Member member1 = factory.createMember("MemberName2");
        Bug bug = factory.createBug(TestConstants.TITLE_SAMPLE, TestConstants.DESCRIPTION_SAMPLE);
        team.addBoard(board);
        board.addBug(bug);
        bug.assignMember(member);
        bug.assignMember(member1);
        Assert.assertEquals("MemberName2", bug.getAssignee().getName());
    }

}
