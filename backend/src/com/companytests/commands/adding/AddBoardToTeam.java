package com.companytests.commands.adding;

import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.models.team.contracts.Board;
import com.company.models.team.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AddBoardToTeam {
    private Factory factory;
    private Engine engine;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
    }

    @Test
    public void addBoardWhenBoardIsValid() {
        Team team = factory.createTeam("TesterName");
        Board board = factory.createBoard("TestName");
        Assert.assertEquals(0, team.getBoards().size());
        team.addBoard(board);
        Assert.assertEquals(1, team.getBoards().size());
    }
}
