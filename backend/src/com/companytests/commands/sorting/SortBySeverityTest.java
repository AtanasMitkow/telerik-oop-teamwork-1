package com.companytests.commands.sorting;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.enumerators.Priority;
import com.company.enumerators.Severity;
import com.company.models.workItems.contracts.Bug;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SortBySeverityTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test()
    public void createBugsAndSortThemBySeverity() {
        Bug bug1 = factory.createBug("BugTitleNumber1", "BugDescriptionNumber1");
        engine.getBugs().add(bug1);
        String bug1id = bug1.getID();
        bug1.changeSeverity(Severity.Major);
        Bug bug2 = factory.createBug("BugTitleNumber2", "BugDescriptionNumber2");
        engine.getBugs().add(bug2);
        String bug2id = bug2.getID();
        bug2.changeSeverity(Severity.Critical);
        Bug bug3 = factory.createBug("BugTitleNumber3", "BugDescriptionNumber3");
        engine.getBugs().add(bug3);
        String bug3id = bug3.getID();
        bug3.changeSeverity(Severity.Minor);
        List<String> parameters = commandParser.parseParameters("sortitemsbyseverity");
        Command testCommand = commandParser.parseCommand("sortitemsbyseverity");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals( "ID: " + bug2id +",\n" +
                "Title: BugTitleNumber2,\n" +
                "Description: BugDescriptionNumber2\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: CriticalID: " + bug1id +",\n" +
                "Title: BugTitleNumber1,\n" +
                "Description: BugDescriptionNumber1\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: MajorID: " + bug3id +",\n" +
                "Title: BugTitleNumber3,\n" +
                "Description: BugDescriptionNumber3\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Minor", result);
    }
}
