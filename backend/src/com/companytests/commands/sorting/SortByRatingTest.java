package com.companytests.commands.sorting;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.workItems.contracts.Feedback;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SortByRatingTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test()
    public void createFeedBackAndSortByRating() {
        Feedback feedback1 = factory.createFeedback("FeedBackTitleNumber1", "FeedBackDescriptionNumber1", 20);
        engine.getFeedbacks().add(feedback1);
        String feedback1ID = feedback1.getID();
        Feedback feedback2 = factory.createFeedback("FeedBackTitleNumber2", "FeedBackDescriptionNumber2", 15);
        engine.getFeedbacks().add(feedback2);
        String feedback2ID = feedback2.getID();
        Feedback feedback3 = factory.createFeedback("FeedBackTitleNumber3", "FeedBackDescriptionNumber3", 35);
        engine.getFeedbacks().add(feedback3);
        String feedback3ID = feedback3.getID();
        List<String> parameters = commandParser.parseParameters("sortitemsbyrating");
        Command testCommand = commandParser.parseCommand("sortitemsbyrating");
        String result = testCommand.execute(parameters,finder);
        Assert.assertEquals( "ID: " + feedback3ID +",\n" +
                "Title: FeedBackTitleNumber3,\n" +
                "Description: FeedBackDescriptionNumber3\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Rating: 35\n" +
                "ID: " + feedback1ID +",\n" +
                "Title: FeedBackTitleNumber1,\n" +
                "Description: FeedBackDescriptionNumber1\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Rating: 20\n" +
                "ID: " + feedback2ID +",\n" +
                "Title: FeedBackTitleNumber2,\n" +
                "Description: FeedBackDescriptionNumber2\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Rating: 15", result);
    }
}
