package com.companytests.commands.sorting;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.models.workItems.contracts.Bug;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SortByTitleTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test()
    public void createBugsAndSortThemByTitle() {
        Bug bug1 = factory.createBug("bBugTitleNumber1", "BugDescriptionNumber1");
        engine.getBugs().add(bug1);
        String bug1id = bug1.getID();
        Bug bug2 = factory.createBug("aBugTitleNumber2", "BugDescriptionNumber2");
        engine.getBugs().add(bug2);
        String bug2id = bug2.getID();
        Bug bug3 = factory.createBug("cBugTitleNumber3", "BugDescriptionNumber3");
        engine.getBugs().add(bug3);
        String bug3id = bug3.getID();
        List<String> parameters = commandParser.parseParameters("sortitemsbytitle");
        Command testCommand = commandParser.parseCommand("sortitemsbytitle");
        String result = testCommand.execute(parameters,finder);
//        " + bug2id +"
        Assert.assertEquals( "ID: " + bug2id +",\n" +
                "Title: aBugTitleNumber2,\n" +
                "Description: BugDescriptionNumber2\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not setID: " + bug1id +",\n" +
                "Title: bBugTitleNumber1,\n" +
                "Description: BugDescriptionNumber1\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not setID: " + bug3id +",\n" +
                "Title: cBugTitleNumber3,\n" +
                "Description: BugDescriptionNumber3\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Steps to reproduce: []\n" +
                "Severity: Severity is not set", result);
    }
}
