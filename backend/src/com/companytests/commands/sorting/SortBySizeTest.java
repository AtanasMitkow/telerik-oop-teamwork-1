package com.companytests.commands.sorting;

import com.company.commands.contracts.Command;
import com.company.core.EngineImpl;
import com.company.core.contracts.Engine;
import com.company.core.contracts.Finder;
import com.company.core.factories.Factory;
import com.company.core.factories.FactoryImpl;
import com.company.core.providers.CommandParser;
import com.company.core.providers.ObjectFinder;
import com.company.enumerators.Size;
import com.company.models.workItems.contracts.Story;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SortBySizeTest {
    private Factory factory;
    private Engine engine;
    private CommandParser commandParser;
    private Finder finder;
    @Before
    public void runBeforeTests() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        commandParser = new CommandParser(factory,engine);
        finder = new ObjectFinder(engine);
    }

    @Test()
    public void createStorysAndSortThemBySize() {
        Story story1 = factory.createStory("StoryTitleNumber1", "StoryDescriptionNumber1");
        engine.getStories().add(story1);
        String story1id = story1.getID();
        story1.changeSize(Size.Large);
        Story story2 = factory.createStory("StoryTitleNumber2", "StoryDescriptionNumber2");
        engine.getStories().add(story2);
        String story2id = story2.getID();
        story2.changeSize(Size.Small);
        Story story3 = factory.createStory("StoryTitleNumber3", "StoryDescriptionNumber3");
        engine.getStories().add(story3);
        String story3id = story3.getID();
        story3.changeSize(Size.Medium);
        List<String> parameters = commandParser.parseParameters("sortitemsbysize");
        Command testCommand = commandParser.parseCommand("sortitemsbysize");
        String result = testCommand.execute(parameters, finder);
        Assert.assertEquals("ID: " + story1id +",\n" +
                "Title: StoryTitleNumber1,\n" +
                "Description: StoryDescriptionNumber1\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Size: Large\n" +
                "ID: " + story3id +",\n" +
                "Title: StoryTitleNumber3,\n" +
                "Description: StoryDescriptionNumber3\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Size: Medium\n" +
                "ID: " + story2id +",\n" +
                "Title: StoryTitleNumber2,\n" +
                "Description: StoryDescriptionNumber2\n" +
                "Status: Status is not currently set\n" +
                "Comments: []\n" +
                "History: []\n" +
                "Assignee: No member assigned\n" +
                "Priority: Priority is not set\n" +
                "Size: Small", result);
    }
}
